package de.tuhrig;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RecursiveAction;

public class Mandelbrot extends RecursiveAction {

	private static final long serialVersionUID = 1L;

	private final List<Bound> bounds = new ArrayList<>();
	
	private final Graphics2D g2;

	private final double factor;
	
	public Mandelbrot(Graphics2D g2, double factor) {
		
		this.g2 = g2;
		
		this.factor = factor;
	}
	
	public void compute() {

		for(Bound bound: bounds) {
			
			paintArea(-2, 0.6, bound.a, bound.b);
		}
	}

	public void add(Bound pair) {

		bounds.add(pair);
	}
	
	private void paintArea(double xMinimum, double xMaximum, double iMinimum, double iMaximum) {
		
		int xMin = (int) (xMinimum * factor);
		int xMax = (int) (xMaximum * factor);
		int iMin = (int) (iMinimum * factor);
		int iMax = (int) (iMaximum * factor);
	
		for(int r = xMin; r <= xMax; r++) {
		
			for(int i = iMin; i <= iMax; i++) {
	
				double cX = r/factor;
				double cY = i/factor;

				int mb = mb(cX, cY);

				int drawX = (int) (r + (2 * factor));
				int drawY = (int) (i + (1 * factor));

				if(mb >= 100)
					drawBlack(g2, drawX, drawY);
				else
					drawRed(g2, drawX, drawY, mb);
			}
		}
	}

	private void drawBlack(Graphics2D g2, int x, int y) {
		
		g2.setColor(Color.black);
		g2.drawRect(x, y, 0, 0);
	}
	
	private void drawRed(Graphics2D g2, int x, int y, int mb) {
		
		g2.setColor(new Color(mb * 75));
		g2.drawRect(x, y, 0, 0);
	}
	
	private int mb(double cre, double cim) {
		
		int count;
		
		double x = 0, y = 0, xto2 = 0, yto2 = 0, dist2;
		
		for (count = 0; count <= 150; count++) {
			
			y = x * y;
			y = y + y + cim;
			x = xto2 - yto2 + cre;
			
			xto2 = x * x;
			yto2 = y * y;
			
			dist2 = xto2 + yto2;
			
			if (dist2 >= 100) {
				
				break;
			}
		}
		
		return count;
	}
}